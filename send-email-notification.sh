#!/bin/bash

# Replace these variables with your actual data
EMAIL_RECIPIENT="the.sako@gmail.com"
EMAIL_SUBJECT="Merge Request Merged"
EMAIL_BODY="A merge request has been merged into the staging branch."

# Using mail command to send the email
echo "$EMAIL_BODY" | mail -s "$EMAIL_SUBJECT" "$EMAIL_RECIPIENT"
